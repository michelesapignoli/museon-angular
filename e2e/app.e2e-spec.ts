import { MuseONPage } from './app.po';

describe('muse-on App', () => {
  let page: MuseONPage;

  beforeEach(() => {
    page = new MuseONPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
