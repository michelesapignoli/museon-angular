import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { AddComponent } from './components/add/add.component';
import { AddArtistComponent } from './components/add-artist/add-artist.component';
import { ArtistsComponent } from './components/artists/artists.component';


const appRoutes: Routes = [
    { path: 'private-access-m', component: LoginComponent },
    { path: '', redirectTo: '/home/gallery', pathMatch: 'full' },
    {
        path: 'home', component: HomeComponent,
        children: [
            { path: '', redirectTo: 'gallery', pathMatch: 'full' },
            { path: 'gallery', component: GalleryComponent },
            { path: 'detail', component: AddComponent },
            { path: 'detail/:type', component: AddComponent },
            { path: 'artist', component: AddArtistComponent },
            { path: 'artists', component: ArtistsComponent }
        ]
    }

];
export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(appRoutes, { useHash: true });
