import { Component, OnInit } from '@angular/core';
import { environment } from '../environments/environment';
import { Router } from '@angular/router';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title = 'app works! ';

    constructor(public router: Router, db:AngularFireDatabase) { }

    ngOnInit() {
        if (!environment.production)
            this.title += ' Debug! ';
        else
            this.title += ' Production! ';



    }

}
