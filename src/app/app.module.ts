import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import {MatFormFieldModule,MatInputModule,MatButtonModule,MatDialogModule,MatSnackBarModule,MatSnackBar,MatSidenavModule,MatToolbarModule,
  MatTableModule,MatSortModule,MatCardModule,MatMenuModule,MatIconModule,MatSelectModule, MatGridListModule, MatChipsModule, MatTooltipModule, MatCheckboxModule, MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AngularFireModule} from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { AddComponent } from './components/add/add.component';
import { AddArtistComponent } from './components/add-artist/add-artist.component';
import { ArtistsComponent } from './components/artists/artists.component';

import { GalleryService } from './providers/gallery.service';
import { AuthenticationService } from './providers/authentication.service';

import * as firebase from 'firebase';


export const firebaseConfig = {
    apiKey: "AIzaSyAqvZnRJK7AVNWNUMUII7HooF_6p-yKMS8",
    authDomain: "museon-9d49f.firebaseapp.com",
    databaseURL: "https://museon-9d49f.firebaseio.com",
    projectId: "museon-9d49f",
    storageBucket: "museon-9d49f.appspot.com",
    messagingSenderId: "1089916778901"
};

firebase.initializeApp(firebaseConfig);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    GalleryComponent,
    AddComponent,
    AddArtistComponent,
    ArtistsComponent
  ],
  imports: [
      BrowserModule,
      AppRoutingModule,
      HttpModule,
      FormsModule,
      BrowserAnimationsModule,
      AngularFireModule.initializeApp(firebaseConfig),
      AngularFireDatabaseModule, 
      AngularFireAuthModule,
      MatGridListModule,
      MatSelectModule,
      CommonModule,
      MatFormFieldModule,
      MatInputModule,
      MatButtonModule,
      MatDialogModule,
      MatSnackBarModule,
      MatSidenavModule,
      MatToolbarModule,
      MatTableModule,
      MatSortModule,
      MatCardModule,
      MatMenuModule,
      MatIconModule,
      MatSelectModule,
      MatChipsModule,
      MatTooltipModule,
      MatCheckboxModule,
      MatProgressSpinnerModule
  ],
  exports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatSelectModule,
    MatChipsModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatProgressSpinnerModule
  ],
  providers: [GalleryService, AuthenticationService],
  bootstrap: [AppComponent]
})
export class AppModule {}
