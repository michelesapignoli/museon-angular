import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Artist, ArtistNode } from '../../model/artist';
import { Observable } from 'rxjs';
import * as firebase from 'firebase';
import { MatSnackBar } from '@angular/material';
import { GalleryService } from '../../providers/gallery.service';
import { AuthenticationService } from '../../providers/authentication.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-add-artist',
  templateUrl: './add-artist.component.html',
  styleUrls: ['./add-artist.component.css']
})
export class AddArtistComponent implements OnInit {

    @Input() storageFolder: string ='images/artists';

    public detail : Artist;
    private firebaseDetailRef : any;

    public imageLoading : boolean = false;
    public saveLoading : boolean = false;

    public currentTag:string;
    private database: any;
    public showDelete : boolean;

    constructor(private af: AngularFireDatabase, private router: Router, private snackbar : MatSnackBar, public service : GalleryService,
            private _sanitizer : DomSanitizer, public authentication : AuthenticationService) {
            this.database = firebase.database();
    }

    ngOnInit() {
        if(this.service.getCurrentArtist() != null){
            this.detail =  this.service.getCurrentArtist();
            this.firebaseDetailRef = this.af.object('/artists/' + this.detail.key);
            //this._sanitizer.bypassSecurityTrustStyle("url('" + this.detail.url + "')");
            this.service.emptyCurrentArtist();
            this.showDelete = true;
        } else {
            if(!this.authentication.amILogged()){
                this.router.navigate(['home/gallery'])
            }
            this.detail = new Artist();
            this.firebaseDetailRef = this.af.list('artists').push(this.detail);
            this.detail.key = this.firebaseDetailRef.key;
        }  
         
    }

    displayImage() {
        firebase.storage().ref('/images/artists/' + this.detail.key + "." + this.detail.ext).getDownloadURL().then(url => {
                this.detail.url = url;
                this.detail.safeUrl = this._sanitizer.bypassSecurityTrustStyle("url('" + url + "')");
                this.imageLoading = false;
            }).catch(function(error) {
                // Handle any errors
                this.snackbar.open("Error :(", "Close", { duration: 3000 });
            });       
    }

    upload() {
        this.imageLoading = true;
        for (let selectedFile of [(<HTMLInputElement>document.getElementById('file')).files[0]]) {
            console.log(selectedFile);
            let path = `/${this.storageFolder}/${this.detail.key}.` + selectedFile.name.split('.').pop();
            firebase.storage().ref().child(path).put(selectedFile).then(snapshot => {
                console.log('Uploaded!', `/${this.storageFolder}/`);
                this.detail.ext = selectedFile.name.split('.').pop();
                this.detail.imageStoragePath = path;
                this.firebaseDetailRef.set(this.detail);
                this.displayImage();
            });
        }

    }

    deleteImg() {
        this.imageLoading = true;
        firebase.storage().ref().child(this.detail.imageStoragePath).delete().then(() => { 
            this.snackbar.open("Deleted!", "Close", { duration: 3000 });
            this.detail.url = null;
            this.imageLoading = false;
        },(error) => {
            this.snackbar.open("Error :(", "Close", { duration: 3000 });
            this.imageLoading = false;
        });
    }

    push(){
        this.saveLoading = true;
        let artist = new ArtistNode(this.detail);
        this.firebaseDetailRef.set(artist).then(url=>{
                this.snackbar.open("Saved!", "Close", { duration: 3000 });
                this.saveLoading = false;
                this.router.navigate(['home/artists']);
            }).catch(error=> {
                this.saveLoading = false;
                this.snackbar.open("Error :(", "Close", { duration: 3000 });
            });;
    }

    delete(){
        this.firebaseDetailRef.remove().then(res=>{
            this.snackbar.open("Bye!", "Close", { duration: 3000 });
            this.saveLoading = false;
            this.router.navigate(['home/artists']);
        }).catch(error=> {
            this.saveLoading = false;
            this.snackbar.open("Error :(", "Close", { duration: 3000 });
        });;
    }

}
