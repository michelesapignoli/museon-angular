import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { PieceOfArt, PieceOfArtNode } from '../../model/pieces-of-art';
import { Artist } from '../../model/artist';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import * as firebase from 'firebase';
import { MatSnackBar } from '@angular/material';
import { GalleryService } from '../../providers/gallery.service';
import { AuthenticationService } from '../../providers/authentication.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Constants } from '../../model/Constants';
import { RealClass } from '../../model/real-class-enum';
import { ArchaeologicalFind, ArchaeologicalFindNode } from '../../model/archaeological-find';
import { ArchaeologicalPiece } from '../../model/archaeological-piece';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    @Input() storageFolder: string = 'images/pieces';

    public detail: any;
    private firebaseDetailRef: any;

    public showingFabs: boolean = false;

    public imageLoading: boolean = false;
    public saveLoading: boolean = false;

    public currentTag: string;

    public artists: Artist[];
    private database: any;
    public showDelete: boolean;

    private params: Subscription;

    public isArch: boolean = false;


    constructor(private af: AngularFireDatabase, private router: Router, private snackbar: MatSnackBar, public service: GalleryService,
        private _sanitizer: DomSanitizer, private route: ActivatedRoute, public authentication: AuthenticationService) {
        this.database = firebase.database();
        this.artists = new Array();
    }

    ngOnInit() {

        if (this.service.getCurrentPieceOfArt() != null) {
            this.detail = this.service.getCurrentPieceOfArt();
            if (this.detail.realClass && this.detail.realClass == RealClass.ARCHAEOLOGICAL_FIND) {
                this.isArch = true;
            }
            this.firebaseDetailRef = this.af.object('/pieces-of-art/' + this.detail.key);
            //this._sanitizer.bypassSecurityTrustStyle("url('" + this.detail.url + "')");
            this.service.emptyCurrentPieceOfArt();
            this.showDelete = true;
        } else {
            if (!this.authentication.amILogged()) {
                this.router.navigate(['home/gallery'])
            }

            this.params = this.route.params.subscribe(
                (params: Params) => {
                    this.isArch = params["type"] ? true : false;
                }
            );
            this.detail = this.isArch ? new ArchaeologicalFind() : new PieceOfArt();
            this.firebaseDetailRef = this.af.list('pieces-of-art').push(this.detail);
            this.detail.key = this.firebaseDetailRef.key;
            this.detail.artist = new Artist();
        }

        this.detail.realClass = this.isArch ? RealClass.ARCHAEOLOGICAL_FIND : RealClass.PIECE;

        this.database.ref(Constants.artist_node).on('child_added', (childSnapshot, prevChildName) => {
            var artist = childSnapshot.val();
            artist.key = childSnapshot.key;
            this.artists.push(artist);
        });
    }

    displayImage() {
        firebase.storage().ref('/images/pieces/' + this.detail.key + "." + this.detail.ext).getDownloadURL().then(url => {
            this.detail.url = url;
            this.detail.safeUrl = this._sanitizer.bypassSecurityTrustStyle("url('" + url + "')");
            this.imageLoading = false;
        }).catch(function (error) {
            // Handle any errors
            this.snackbar.open("Error :(", "Close", { duration: 3000 });
        });
    }

    upload() {
        this.imageLoading = true;
        for (let selectedFile of [(<HTMLInputElement>document.getElementById('file')).files[0]]) {
            console.log(selectedFile);
            let path = `/${this.storageFolder}/${this.detail.key}.` + selectedFile.name.split('.').pop();
            firebase.storage().ref().child(path).put(selectedFile).then(snapshot => {
                console.log('Uploaded!', `/${this.storageFolder}/`);
                this.detail.ext = selectedFile.name.split('.').pop();
                this.detail.imageStoragePath = path;
                this.firebaseDetailRef.set(this.detail);
                this.displayImage();
            });
        }

    }

    deleteImg() {
        this.imageLoading = true;
        firebase.storage().ref().child(this.detail.imageStoragePath).delete().then(() => {
            this.snackbar.open("Deleted!", "Close", { duration: 3000 });
            this.detail.url = null;
            this.imageLoading = false;
        }, (error) => {
            this.snackbar.open("Error :(", "Close", { duration: 3000 });
            this.imageLoading = false;
        });
    }

    push() {
        this.saveLoading = true;
        let piece = this.isArch ? new ArchaeologicalFindNode(this.detail) : new PieceOfArtNode(this.detail)
        this.firebaseDetailRef.set(piece).then(url => {
            this.snackbar.open("Saved!", "Close", { duration: 3000 });
            this.saveLoading = false;
            this.router.navigate(['home']);
        }).catch(error => {
            this.saveLoading = false;
            this.snackbar.open("Error :(", "Close", { duration: 3000 });
        });
    }

    onTagEnter() {
        if (this.detail.tags == null) {
            this.detail.tags = new Array();
        }
        if (this.detail.tags.length > 4) {
            this.snackbar.open("5 tags max!", "Close", { duration: 3000 });
        } else {
            if (this.currentTag.length < 3) {
                this.snackbar.open("Min. 3 chars for each tag!", "Close", { duration: 3000 });
            } else {
                this.detail.tags.push(this.currentTag);
                this.currentTag = null;
            }
        }
    }

    deleteTag(tag: string) {
        this.detail.tags = this.detail.tags.filter(t => t != tag);
    }

    delete() {
        this.firebaseDetailRef.remove().then(res => {
            this.snackbar.open("Bye!", "Close", { duration: 3000 });
            this.saveLoading = false;
            this.router.navigate(['home']);
        }).catch(error => {
            this.saveLoading = false;
            this.snackbar.open("Error :(", "Close", { duration: 3000 });
        });;
    }

    showFabs() {
        this.showingFabs = !this.showingFabs;
    }

    addArchaelogicalPiece() {
        if (this.isArch) {
            if (this.detail.pieces == null) {
                this.detail.pieces = new Array();
            }
            this.detail.pieces.push(new ArchaeologicalPiece())
        }

    }
}
