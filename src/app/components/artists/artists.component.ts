import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { Artist } from '../../model/artist';
import { Constants } from '../../model/Constants';
import { GalleryService } from '../../providers/gallery.service';
import { AuthenticationService } from '../../providers/authentication.service';
import * as firebase from 'firebase';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RealClass } from '../../model/real-class-enum';

@Component({
    selector: 'app-artists',
    templateUrl: './artists.component.html',
    styleUrls: ['./artists.component.css']
})
export class ArtistsComponent implements OnInit {
    public artists: Artist[] = [];
    private database: any;
    private arrayIndex: number = 0;
    public showingFabs:boolean = false;
    public ARCH_PIECE: RealClass = RealClass.ARCHAEOLOGICAL_FIND;

    constructor(private _sanitizer : DomSanitizer, private router : Router, public service : GalleryService, public authentication : AuthenticationService) {
        this.database = firebase.database();
    }

    ngOnInit() {
        this.database.ref(Constants.artist_node).orderByChild("name").on('child_added', (childSnapshot, prevChildName) => {
            var artist = childSnapshot.val();
            artist.key = childSnapshot.key;
            artist.cols = Constants.cols[this.arrayIndex];
            artist.rows = Constants.rows[this.arrayIndex];
            this.arrayIndex = (this.arrayIndex + 1) % Constants.rows.length;
            artist.safeUrl = this._sanitizer.bypassSecurityTrustStyle("url('" + artist.url + "')");
            this.artists.push(artist);
        });
    }

    open(artist : Artist){
        this.service.setCurrentArtist(artist);
        this.router.navigate(['home/artist']);
    }

    getCols(artist : Artist){
        const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        return width > 720? 1: 4;
    }

    getRows(artist : Artist){
        const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        return width > 720? 1: 4;
    }

    showFabs() {
        this.showingFabs = !this.showingFabs;
    }
}
