import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { PieceOfArt } from '../../model/pieces-of-art';
import { Artist } from '../../model/artist';
import { RealClass } from '../../model/real-class-enum';
import { Constants } from '../../model/Constants';
import { GalleryService } from '../../providers/gallery.service';
import { AuthenticationService } from '../../providers/authentication.service';
import * as firebase from 'firebase';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
    selector: 'app-gallery',
    templateUrl: './gallery.component.html',
    styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
    public pieces: any[] = [];
    private artists: Artist[];
    private database: any;
    private arrayIndex: number = 0;
    public showingFabs: boolean = false;
    public ARCH_PIECE: RealClass = RealClass.ARCHAEOLOGICAL_FIND;
    public isInit: boolean = false;

    constructor(private _sanitizer: DomSanitizer, private router: Router, public service: GalleryService, public authentication: AuthenticationService) {
        this.database = firebase.database();
        this.artists = new Array();
        this.pieces = new Array();
    }

    ngOnInit() {
        this.database.ref(Constants.artist_node).on('child_added', (childSnapshot, prevChildName) => {
            var artist = childSnapshot.val();
            artist.key = childSnapshot.key;
            this.artists.push(artist);
        });

        this.database.ref(Constants.piecesOfArt_node).on('child_added', (childSnapshot, prevChildName) => {
            var piece = childSnapshot.val();
            piece.key = childSnapshot.key;
            piece.cols = Constants.cols[this.arrayIndex];
            piece.rows = Constants.rows[this.arrayIndex];
            if (piece.artist != null) {
                var artist = this.artists.find(a => a.key == piece.artist.key);
                if (artist != null) {
                    piece.artist = artist
                }
            }

            this.arrayIndex = (this.arrayIndex + 1) % Constants.rows.length;
            piece.safeUrl = this._sanitizer.bypassSecurityTrustStyle("url('" + piece.url + "')");
            this.pieces.push(piece);
        });
        this.isInit = false;
    }

    open(piece: any) {
        this.service.setCurrentPieceOfArt(piece);
        this.router.navigate(['home/detail']);
    }

    getCols(piece: PieceOfArt) {
        const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        return width > 720 ? (width > 1080 ? piece.cols : 2) : 4;
    }

    getRows(piece: PieceOfArt) {
        const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        return width > 720 ? (width > 1080 ? piece.rows : 2) : 4;
    }

    showFabs() {
        this.showingFabs = !this.showingFabs;
    }
}
