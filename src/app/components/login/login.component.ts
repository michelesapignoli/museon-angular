import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { MatSnackBar } from '@angular/material';
import * as firebase from 'firebase';
import * as firebaseui from 'firebaseui';
import { AuthenticationService } from '../../providers/authentication.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    public firebaseUiInstance:any;
    
    constructor(public router: Router, public afAuth: AngularFireAuth, public snackBar: MatSnackBar, public authentication : AuthenticationService) { }
    ngOnInit() {
        if(!this.firebaseUiInstance){
            this.firebaseUIPopup();
        }
        
    }
    firebaseUIPopup() {
        this.firebaseUiInstance = this.authentication.getFirebaseUI();
        let uiConfig = {
            callbacks: {
                signInSuccess: (currentUser, credential, rederictUrl) => {
                    this.snackBar.open("Welcome!", "Close", { duration: 3000 });
                    this.router.navigate(['/home']);
                    return false;
                }
            },

            credentialHelper: firebaseui.auth.CredentialHelper.NONE,
            signInFlow: 'popup',
            signInOptions: [
                // Leave the lines as is for the providers you want to offer your users.
                //firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                //firebase.auth.FacebookAuthProvider.PROVIDER_ID,
                //firebase.auth.PhoneAuthProvider.PROVIDER_ID,
                firebase.auth.EmailAuthProvider.PROVIDER_ID
            ],
            // Terms of service url.
            tosUrl: '<your-tos-url>'
        };
        // The start method will wait until the DOM is loaded.
        this.firebaseUiInstance.start('#firebaseui-auth-container', uiConfig);


    }

}