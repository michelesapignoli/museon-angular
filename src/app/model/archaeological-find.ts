import { CulturalAsset } from "./cultural-asset";
import { ArchaeologicalPiece } from "./archaeological-piece";
import { RealClass } from "./real-class-enum";

export class ArchaeologicalFind extends CulturalAsset {
    type: string
    epoch: string
    time: string
    owner: string
    dateOfDiscovery: string
    placeOfDiscovery: string
    pieces: [ArchaeologicalPiece]
    tags:string[]
    description:string
}

export class ArchaeologicalFindNode {
    
        key: string;
        title: string;
        caption: string;
        type: string
        epoch: string
        time: string
        owner: string
        dateOfDiscovery: string
        placeOfDiscovery: string
        pieces: [ArchaeologicalPiece]
    
        imageStoragePath:string;
        url:string;
        ext:string;
    
        beacon_uuid:string;
        beacon_major:string;
        beacon_minor:string;

        realClass:RealClass;
    
        tags:string[];

        description:string
    
        constructor(piece:ArchaeologicalFind) {
            this.key = piece.key != null? piece.key : null;
            this.title = piece.title != null? piece.title : null;
            this.caption = piece.caption != null? piece.caption : null;
            this.type = piece.type != null? piece.type : null;
            this.epoch = piece.epoch != null? piece.epoch : null;
            this.time = piece.time != null? piece.time : null;
            this.owner=piece.owner != null? piece.owner : null;
            this.dateOfDiscovery = piece.dateOfDiscovery != null? piece.dateOfDiscovery : null;
            this.placeOfDiscovery = piece.placeOfDiscovery != null? piece.placeOfDiscovery : null;
            this.pieces = piece.pieces!=null? piece.pieces : null;
            this.imageStoragePath = piece.imageStoragePath != null? piece.imageStoragePath : null;
            this.url = piece.url != null? piece.url : null;
            this.ext = piece.ext != null? piece.ext : null;
            this.beacon_uuid = piece.beacon_uuid != null? piece.beacon_uuid : null;
            this.beacon_major = piece.beacon_major != null? piece.beacon_major : null;
            this.beacon_minor = piece.beacon_minor != null? piece.beacon_minor : null;
            this.tags = piece.tags != null? piece.tags : null;
            this.realClass = piece.realClass != null? piece.realClass : null
            this.description = piece.description != null? piece.description:null
        }
    
    }