export class ArchaeologicalPiece {
    key: string
    title: string
    size: string
    hostedAt: string
    ARasset: string
    picture: string
    inMuseum: boolean
}