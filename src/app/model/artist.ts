export class Artist {

    key: string;
    name: string;
    city: string;
    born: string;
    dead: string;
    art_movement: string;
    description: string;

    imageStoragePath:string;
    effect:string;
    url:string;
    safeUrl:any;
    ext:string;

    constructor() {

    }

}

export class ArtistNode {
    
        key: string;
        name: string;
        city: string;
        born: string;
        dead: string;
        art_movement: string;
        description: string;  
        imageStoragePath:string;
        url:string;
        ext:string;
    
        constructor(artist : Artist) {
            this.key = artist.key != null? artist.key:null;
            this.name = artist.name != null? artist.name:null;
            this.city = artist.city != null? artist.city:null;
            this.born = artist.born != null? artist.born:null;
            this.dead = artist.dead != null? artist.dead:null;
            this.art_movement = artist.art_movement != null? artist.art_movement:null;
            this.description = artist.description != null? artist.description:null;
            this.imageStoragePath = artist.imageStoragePath != null? artist.imageStoragePath:null;
            this.url = artist.url != null? artist.url:null;
            this.ext = artist.ext != null? artist.ext:null;
        }
    
    }


