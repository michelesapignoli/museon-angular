export class Constants{
    public static cols: number[] = [1, 2, 1, 1, 1, 2, 1, 2, 2, 1, 1];
    public static rows: number[] = [1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 1];
    public static piecesOfArt_node : string = '/pieces-of-art';
    public static artist_node : string = '/artists';
    public static cols_m: number[] = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2];
    public static rows_m: number[] = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2];

}


