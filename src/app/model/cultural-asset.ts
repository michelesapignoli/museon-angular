import { RealClass } from "./real-class-enum";

export class CulturalAsset {
    
        key: string;
        title: string;
        caption: string;
    
        imageStoragePath:string;
        effect:string;
        url:string;
        safeUrl:any;
        ext:string;
    
        cols: number;
        rows: number;
    
        beacon_uuid:string;
        beacon_major:string;
        beacon_minor:string;

        realClass:RealClass;
    
        constructor() {
    
        }
    
    }