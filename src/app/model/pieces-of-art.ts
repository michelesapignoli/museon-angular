import { Artist } from './artist';
import { CulturalAsset } from './cultural-asset';
import { RealClass } from './real-class-enum';

export class PieceOfArt extends CulturalAsset {

    artist: Artist;
    date: string;
    technique: string;
    dimensions: string;
    art_movement: string;
    description: string;
    location: string;

    tags:string[];

    constructor() {
        super();
    }

}

export class PieceOfArtNode {
    
        key: string;
        title: string;
        artist: string;
        date: string;
        technique: string;
        dimensions: string;
        art_movement: string;
        description: string;
        location: string;
        beacon: string;
        rank: string;
    
        imageStoragePath:string;
        url:string;
        ext:string;

        realClass:RealClass;
    
        beacon_uuid:string;
        beacon_major:string;
        beacon_minor:string;
    
        tags:string[];
    
        constructor(piece:PieceOfArt) {
            this.key = piece.key != null? piece.key : null;
            this.title = piece.title != null? piece.title : null;
            this.artist = piece.artist.key != null? piece.artist.key : null;
            this.date = piece.date != null? piece.date : null;
            this.technique = piece.technique != null? piece.technique : null;
            this.dimensions = piece.dimensions != null? piece.dimensions : null;
            this.art_movement=piece.art_movement != null? piece.art_movement : null;
            this.description = piece.description != null? piece.description : null;
            this.location = piece.location != null? piece.location : null;
            this.imageStoragePath = piece.imageStoragePath != null? piece.imageStoragePath : null;
            this.url = piece.url != null? piece.url : null;
            this.ext = piece.ext != null? piece.ext : null;
            this.beacon_uuid = piece.beacon_uuid != null? piece.beacon_uuid : null;
            this.beacon_major = piece.beacon_major != null? piece.beacon_major : null;
            this.beacon_minor = piece.beacon_minor != null? piece.beacon_minor : null;
            this.tags = piece.tags != null? piece.tags : null;
            this.realClass = piece.realClass != null? piece.realClass : null
        }
    
    }


