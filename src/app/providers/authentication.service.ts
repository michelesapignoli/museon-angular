import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebaseui from 'firebaseui';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class AuthenticationService {
  private firebaseUI: any;

  constructor(public af: AngularFireAuth, public router: Router, public snackBar: MatSnackBar) {
    this.initializeFirebaseUI()
  }

  public amILogged() {
    var user = firebase.auth().currentUser
    return user ? true : false;
  }

  public logout() {
    if (this.af.auth.currentUser) {
      this.af.auth.signOut().then(() => {
        this.snackBar.open("Logged out!", "Close", { duration: 3000 });
      })
    } else {
      this.router.navigate(['private-access-m'])
    }

  }

  public initializeFirebaseUI() {
    this.firebaseUI = new firebaseui.auth.AuthUI(firebase.auth())
  }

  public getFirebaseUI() {
    return this.firebaseUI
  }

}
