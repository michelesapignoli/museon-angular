import { Injectable } from '@angular/core';
import { PieceOfArt } from '../model/pieces-of-art';
import { Artist } from '../model/artist';

@Injectable()
export class GalleryService {

  private currentPieceOfArt : any;
  private currentArtist : Artist;

  constructor() {} 

  public setCurrentPieceOfArt(piece : any){
    this.currentPieceOfArt = piece;
  }

  public emptyCurrentPieceOfArt(){
    this.currentPieceOfArt = null;
  }

  public getCurrentPieceOfArt(){
    return this.currentPieceOfArt;
  }

   public setCurrentArtist(artist : Artist){
    this.currentArtist = artist;
  }

  public emptyCurrentArtist(){
    this.currentArtist = null;
  }

  public getCurrentArtist(){
    return this.currentArtist;
  }
}
